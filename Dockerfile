FROM docker:latest

RUN apk add python3 && \
  pip3 install --upgrade pip && \
  pip3 install awscli --upgrade --user && \
  echo export PATH=~/.local/bin:\$PATH >> ~/.profile